dojo._xdResourceLoaded(function(dojo, dijit, dojox){
return {depends: [["provide", "dojo.nls.fw_wem_login_it"],
["provide", "dijit.form.nls.validate"],
["provide", "dijit.form.nls.validate.it"]],
defineResource: function(dojo, dijit, dojox){dojo.provide("dojo.nls.fw_wem_login_it");dojo.provide("dijit.form.nls.validate");dijit.form.nls.validate._built=true;dojo.provide("dijit.form.nls.validate.it");dijit.form.nls.validate.it={"rangeMessage":"Questo valore non è compreso nell'intervallo.","invalidMessage":"Il valore immesso non è valido.","missingMessage":"Questo valore è obbligatorio."};

}};});