<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="COM.FutureTense.Interfaces.Utilities"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<% 
		String cssLocation=Utilities.goodString(request.getParameter("wemLoginCss")) ? 
							StringEscapeUtils.escapeHtml(request.getParameter("wemLoginCss")) : "" ;
		if(cssLocation!=null)request.setAttribute("cssLocation",cssLocation);

		String htmlLocation=request.getParameter("wemLoginTemplate");
		   
		if(htmlLocation != null)
		{
			request.setAttribute("skin",htmlLocation);
			try
			{	
				URL customFormUrl=new URL(htmlLocation);
			        URLConnection conn = customFormUrl.openConnection();
	                        conn.setDoOutput(false);
			        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			        String line;
			        String resString = "";
		                while ((line = rd.readLine()) != null)
		 	        {
			            resString += line;
			        }
		                rd.close();			
				request.setAttribute("customForm",StringEscapeUtils.escapeHtml(resString));
			}catch(Exception ex)
			{
				request.setAttribute("skin",null);	
				request.setAttribute("waserror",true);
			}
			
		}
%>   
	<head>
		<title><spring:message code="browser.wem.login.title"/></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:choose>
	<c:when test="${empty cssLocation}">
	   	<style type="text/css" media="screen">
			@import 'fatwire/css/login.css';
		</style>
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" href="${cssLocation}" type="text/css">
	</c:otherwise>
	
	</c:choose>
		<script type="text/javascript" src="js/common_rosters.js"></script>
		<link rel="shortcut icon" href="<%= request.getContextPath() %>/sitesFavicon.ico" type="image/x-icon" />
	</head>

	<body id="cas" onload="init();">
		<form:form method="post" id="fm1" cssClass="fm-v clearfix" commandName="${commandName}" htmlEscape="true">
<%--
Potential TODO: provide a way to customize location of error feedback
within templates?  For now I am including the form:errors tag manually in the
case of custom template, but in the case of our default, it is specified inside
c_defaultForm.jsp so that we can place it where we want.  (RE #21463)
--%>
			<c:choose>
			<c:when test="${empty skin}">
				<jsp:directive.include file="c_defaultForm.jsp" />
			</c:when>
			<c:otherwise>
				<form:errors path="*" cssClass="errors" id="status" element="div" />
				${customForm}
			</c:otherwise>
			</c:choose>
			<c:if test="${not empty sessionScope.openIdLocalId}">
				<input type="hidden" id="username" name="username" value="${sessionScope.openIdLocalId}" />
			</c:if>
			<input type="hidden" name="lt" value="${flowExecutionKey}" />
			<input type="hidden" name="_eventId" value="submit" />
			<div style="display: none;"><input id="warn" name="warn" value="true" tabindex="3" type="checkbox" /></div>
		</form:form>
		<c:if test="${not empty waserror}">
		<div id="errorbar">
			Could not retrieve skin. Skin is specified in cas.properties file as parameter "wemLoginTemplate" at cas.filter.auth.login.serverUrl property URL
		</div>
		</c:if>
	</body>
</html>
