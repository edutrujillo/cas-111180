dojo._xdResourceLoaded(function(dojo, dijit, dojox){
return {depends: [["provide", "dojo.nls.fw_wem_login_en"],
["provide", "dijit.form.nls.validate"],
["provide", "dijit.form.nls.validate.en"]],
defineResource: function(dojo, dijit, dojox){dojo.provide("dojo.nls.fw_wem_login_en");dojo.provide("dijit.form.nls.validate");dijit.form.nls.validate._built=true;dojo.provide("dijit.form.nls.validate.en");dijit.form.nls.validate.en={"rangeMessage":"This value is out of range.","invalidMessage":"The value entered is not valid.","missingMessage":"This value is required."};

}};});