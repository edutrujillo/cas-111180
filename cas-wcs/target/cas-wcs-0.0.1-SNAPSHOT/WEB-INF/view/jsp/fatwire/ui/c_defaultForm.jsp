<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.io.InputStream" %>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="COM.FutureTense.Interfaces.Utilities"%>
<%-- dojo + layer --%>
<script type="text/javascript" src="fatwire/js/dojo/dojo.js"></script>
<script type="text/javascript" src="fatwire/js/dojo/fw_wem_login.js"></script>
<%-- login js with requires - unnecessary with layer, actually...--%>
<script type="text/javascript" src="fatwire/js/login.js"></script>
<%
//[#21454] Retrieve "forgot password" e-mail from fatwire_settings.properties
Properties fwprops = new Properties();
try {
	ClassLoader loader = this.getClass().getClassLoader();
	InputStream istream = loader.getResourceAsStream("fatwire_settings.properties");
	fwprops.load(istream);
} catch (Exception e) {
	//somehow our properties file is missing; set default
	fwprops.setProperty("forgotpassword", "admin@localhost");
}
%>
<script type="text/javascript">
	var cookieName = 'WEMUI:rememberme';
	
	function onSubmitClick() {
		//handle setting of "remember me" preference on submit
		//(dojo.connect doesn't seem to cut it...)
		if (dojo.byId('remember').checked) {
			dojo.cookie(cookieName, dijit.byId('username').attr('value'), {
				expires: 365,
				path: '<%=request.getContextPath()%>'
			});
		} else {
			//clear out cookie in case it was previously set
			dojo.cookie(cookieName, null, {
				expires: -1,
				path: '<%=request.getContextPath()%>'
			});
		}
	}
	
	dojo.addOnLoad(function() {
		dojo.addClass(dojo.body(), 'fw');		
		
		//[KGF #21652] hide login form content until we've added in our fw class
		dojo.style('container', 'display', '');
		dojo.parser.parse('loginbox');
		
		//check for "remember me" preference
		var remember = dojo.cookie(cookieName);
		if (remember) {
			//NOTE: Remember Me functionality overrides browser remember-password!
			dijit.byId('username').attr('value', remember);
			dojo.byId('remember').checked = true;
			dijit.byId('password').focus();
		} else {
			dijit.byId('username').focus();
		}
	});
</script>
<div class="centerboxcontainer" id="container" style="display: none">
	<div class="centerbox">
		<div class="loginbox" id="loginbox">
			<div class="topshadow"></div>
			<div class="leftshadow"></div>
			<div class="rightshadow"></div>
			<div class="bottomshadow"></div>

			<div class="loginBevelBottom"></div>
			
			<div class="loginheader"><div class="item logoWebCenterSites"><spring:message code="screen.login.label.version"/></div></div>
			<div id="timeoutError" class="errors" style="display:none;background-color: rgb(255, 238, 221);">
				<spring:message code="screen.login.error.timeout"/>
			</div>
			<form:errors path="*" cssClass="errors" id="status" element="div" />
			<div class="logincontent">
				<div class="logo"></div>
				<div class="loginform">
					<div style="display: none">
<%-- [KGF #21865] preliminary DOM nodes to enable browser password saving --%>
						<input type="text" name="username"/>
						<input type="password" name="password"/>
					</div>
						<div class="right"><b><spring:message code="screen.login.header"/></b></div>						
						<div class="clear"></div>
						<div class="pt40"></div>
						<div class="right"><div dojoType="fw.dijit.UIInput"
						 autocomplete="on" id="username" name="ac_username"<%=
(Utilities.goodString(request.getParameter("username")) ?
	" value=\"" + StringEscapeUtils.escapeHtml(request.getParameter("username")) + "\"" : "")
						 %>></div></div>
						<div class="title right"><spring:message code="screen.login.label.username"/></div>
						<div class="clear"></div>
						<div class="pt10"></div>
						<div class="right"><div dojoType="fw.dijit.UIInput"
						 autocomplete="on" id="password" name="ac_password" type="password"></div></div>		
						<div class="title right"><spring:message code="screen.login.label.password"/></div>
						<div class="clear"></div>
						<div class="right forgot"><a href="mailto:<%=fwprops.getProperty("forgotpassword")%>"><spring:message code="screen.login.label.forgotpassword"/></a></div>
						<div class="clear"></div>						
						<div class="pt10"></div>
						<div class="right">
							<input type="submit" value="" class="loginbtn" />
						</div>
						 <div style="margin-left: 90px;">
							<button dojoType="fw.ui.dijit.Button" buttonStyle="grey" onClick="document.forms['fm1'].submit(); onSubmitClick();">
								<spring:message code="screen.welcome.button.login"/>
							</button>
						</div>
						<div class="clear"></div>
						<div class="pt10"></div>
						<div class="right"><input type="checkbox" name="remember" id="remember"/> <spring:message code="screen.login.label.rememberme"/></div>

				</div>
			</div>
		</div><%-- end of loginbox --%>
		<div class="center copyright">
			<spring:message code="screen.label.copyrighttext"/><br />
			<spring:message code="screen.label.trademarktext"/>
		</div>
	</div><%-- end of centerbox --%>
</div>
<div class="loginscreen"></div>
<div class="loginscreenbg"></div>

<!-- 16528590: on session timeout, timeouterror page takes to the login page with a message indicating session timeout (instead of previously showing the timeout error page) -->
<%
String referrer = request.getHeader("referer");
if(referrer != null && referrer.contains("pagename=OpenMarket%2FXcelerate%2FActions%2FSecurity%2FTimeoutError")){
	%>
	<script type="text/javascript">
	dojo.byId('timeoutError').style.display='';
	</script>
<%
}

%>