<%@page session="false"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>

<cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'>
	<cas:authenticationSuccess>
		<c:set var="user"
			value="${assertion.chainedAuthentications[fn:length(assertion.chainedAuthentications)-1].principal.id}"
			scope="page" />
		<cas:user><%=org.apache.commons.lang.StringEscapeUtils.escapeXml((java.lang.String)pageContext.getAttribute("user"))%></cas:user>

		<c:if test="${not empty pgtIou}">
			<cas:proxyGrantingTicket>${pgtIou}</cas:proxyGrantingTicket>
		</c:if>
		<c:if test="${fn:length(assertion.chainedAuthentications) > 1}">
			<cas:proxies>
				<c:forEach var="proxy" items="${assertion.chainedAuthentications}"
					varStatus="loopStatus" begin="0"
					end="${fn:length(assertion.chainedAuthentications)-2}" step="1">
					<cas:proxy>${fn:escapeXml(proxy.principal.id)}</cas:proxy>
				</c:forEach>
			</cas:proxies>
		</c:if>
	</cas:authenticationSuccess>
	
	<c:set var="map"
		value="${assertion.chainedAuthentications[fn:length(assertion.chainedAuthentications)-1].principal.attributes}"
		scope="page" />
	<cas:attributes>
		<%
			Map map = (Map) pageContext.getAttribute("map");

					Iterator i = map.keySet().iterator();
					while (i.hasNext()) {
						Object key = i.next();
						Object value = map.get(key);
		%>
		<cas:<%=key%>><%=org.apache.commons.lang.StringEscapeUtils.escapeXml((java.lang.String)value)%></cas:<%=key%>>
		<%
			}
		%>
	</cas:attributes>
</cas:serviceResponse>
